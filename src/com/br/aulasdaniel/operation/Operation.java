package com.br.aulasdaniel.operation;

import com.br.aulasdaniel.conectaBanco.Conection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Operation {

    public void insere(int pId, String pNome, int pPhone, String pEstado, String pEmail) {
         Conection c = new Conection();
     Connection cn = c.getConnection();
    PreparedStatement smt;
        String sql = "INSERT INTO tb_cliente VALUES (?, ?, ?, ?, ?)";
        
        
        try {
            smt = cn.prepareStatement(sql);
            smt.setString(1, Integer.toString(pId));
            smt.setString(2, pNome);
            smt.setString(3, Integer.toString(pPhone));
            smt.setString(4, pEstado);
            smt.setString(5, pEmail);
            smt.executeUpdate();
            smt.close();
            JOptionPane.showMessageDialog(null, "Inseriu");
        } catch (SQLException sq) {

            JOptionPane.showMessageDialog(null, sq.getMessage());
        }
    }
    
    public void deletar (int pId){
        
         Conection c = new Conection();
    Connection cn = c.getConnection();
    PreparedStatement smt;
     String del = "DELETE FROM tb_cliente WHERE idCli = ?";
  
    try {
        smt = cn.prepareStatement(del);
        smt.setInt(1, pId);

            smt.executeUpdate();
            smt.close();
            JOptionPane.showMessageDialog(null, "Deletado");

    } catch (SQLException e1){
            JOptionPane.showMessageDialog(null, "Error" + e1.getMessage());
    }
    }
}
