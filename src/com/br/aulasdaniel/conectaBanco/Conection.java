package com.br.aulasdaniel.conectaBanco;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conection {

    private Statement smt;
    private String user, password, hostname, database, url;
    private Connection cnt;

    private int port;

    public Conection() {

        try {
            hostname = "localhost";
            port = 3306;
            database = "cruddibb";
            user = "root";
            password = "";
            url = "jdbc:mysql://" + hostname + ":" + port + "/" + database;
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            cnt = (Connection) DriverManager.getConnection(url, user, password);
            System.out.println("Conectado com sucesso!");

        } catch (SQLException ex) {
            System.out.println(" Erro de conexão :( " + ex.getMessage());
        }

    }

    public Connection getConnection() {
        return cnt;
    }

    public void desconectar() {
        try {
            cnt.close();
        } catch (SQLException e2) {
            System.out.println("\nNão foi possível desconectar\n" + e2.getMessage());
        }
    }

}
